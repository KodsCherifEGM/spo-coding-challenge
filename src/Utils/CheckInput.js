const regExpName = /^[a-zA-Z]+$/ig
const regExpUsername = /^[a-z0-9._]+$/g
const regExpEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i

//check the firstname
export const checkName = (input) => {
    var msg = ""
    if (input == "")
        msg = "this field is required"
    else
        if (regExpName.test(input) == false)
            msg = "not valid data"
        else
            msg = "";

    return msg;
}

//check the username 
export const checkUsername = (input) => {
    var msg = ""
    if (input == "")
        msg = "this field is required"
    else
        if (regExpUsername.test(input) == false)
            msg = "not valid data"
        else
            msg = "";

    return msg;
}
//check the password 
export const checkPassword = (input) => {
    var msg = ""
    if (input == "")
        msg = "this field is required"
    else
        if (input.length < 8)
            msg = "not valid data"
        else
            msg = "";

    return msg;
}
//check the email
export const checkEmail = (input) => {
    var msg = ""
    if (input == "")
        msg = "this field is required"

    else
        if (regExpEmail.test(input) == false)
            msg = "not valid data"
        else
            msg = "";

    return msg;

}