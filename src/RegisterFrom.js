import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Alert } from 'react-native';
import { checkName, checkUsername, checkPassword, checkEmail } from './Utils/CheckInput'
class RegisterForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            firstname: '',
            lastname: '',
            username: '',
            password: '',
            email: '',
            errorfirstname: '',
            errorlastname: '',
            errorusername: '',
            errorpassword: '',
            erroremail: '',
        };
    }

    //here we check if the input conatains the right data
    checkInput(inputText, type) {

        if (type == "firstname") this.setState({ errorfirstname: checkName(inputText) })
        if (type == "lastname") this.setState({ errorlastname: checkName(inputText) })
        if (type == "username") this.setState({ errorusername: checkUsername(inputText) })
        if (type == "password") this.setState({ errorpassword: checkPassword(inputText) })
        if (type == "email") this.setState({ erroremail: checkEmail(inputText) })
    }

    //here also we check if the inputs contains the right data when we click on submit button
    checkSubmit() {
        if (checkName(this.state.firstname) == ""
            && checkName(this.state.lastname) == ""
            && checkUsername(this.state.username) == ""
            && checkPassword(this.state.password) == ""
            && checkEmail(this.state.email) == "") {
            this.setState({
                errorfirstname: "",
                errorlastname: "",
                errorusername: "",
                errorpassword: "",
                erroremail: "",
            })
            Alert.alert("Welcome", "Thank you for joining us " + this.state.firstname + " " + this.state.lastname + ", your username : " + this.state.username + " , email : " + this.state.email, [{ text: "Thanks" }])
        }
        else {

            this.setState({ errorfirstname: checkName(this.state.firstname) })
            this.setState({ errorlastname: checkName(this.state.lastname) })
            this.setState({ errorusername: checkUsername(this.state.username) })
            this.setState({ errorpassword: checkPassword(this.state.password) })
            this.setState({ erroremail: checkEmail(this.state.email) })

        }
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.signInBar}>
                    <Text style={styles.signInBarText}>Create Your Free Account</Text>
                </View>
                <View style={styles.form}>
                    <View style={styles.inputContainer}>
                        <Text>First Name</Text>
                        <TextInput
                            placeholder='First Name'
                            autoCapitalize='none'
                            autoCorrect={false}
                            style={styles.textInput}
                            underlineColorAndroid='transparent'
                            value={this.state.firstname}
                            onChangeText={(text) => this.setState({ firstname: text })}
                            onBlur={() => this.checkInput(this.state.firstname, "firstname")} />
                        <Text style={styles.errorText}>{this.state.errorfirstname}</Text>
                    </View>
                    <View style={styles.inputContainer}>
                        <Text>Last Name</Text>
                        <TextInput
                            placeholder='Last Name'
                            autoCapitalize='none'
                            autoCorrect={false}
                            style={styles.textInput}
                            underlineColorAndroid='transparent'
                            value={this.state.lastname}
                            onChangeText={(text) => this.setState({ lastname: text, username: this.state.firstname + text })}
                            onBlur={() => this.checkInput(this.state.lastname, "lastname")} />
                        <Text style={styles.errorText}>{this.state.errorlastname}</Text>
                    </View>
                    <View style={styles.inputContainer}>
                        <Text>Username</Text>
                        <TextInput
                            placeholder='Username'
                            autoCapitalize='none'
                            autoCorrect={false}
                            style={styles.textInput}
                            underlineColorAndroid='transparent'
                            value={this.state.username}
                            onChangeText={(text) => this.setState({ username: text })}
                            onBlur={() => this.checkInput(this.state.username, "username")} />
                        <Text style={styles.errorText}>{this.state.errorusername}</Text>
                    </View>
                    <View style={styles.inputContainer}>
                        <Text>Password</Text>
                        <TextInput
                            placeholder='Password'
                            autoCapitalize='none'
                            autoCorrect={false}
                            secureTextEntry={true}
                            style={styles.textInput}
                            underlineColorAndroid='transparent'
                            value={this.state.password}
                            onChangeText={(text) => this.setState({ password: text })}
                            onBlur={() => this.checkInput(this.state.password, "password")} />
                        <Text style={styles.hintText}>At least 8 characters</Text>
                        <Text style={styles.errorText}>{this.state.errorpassword}</Text>
                    </View>
                    <View style={styles.inputContainer}>
                        <Text>Email</Text>
                        <TextInput
                            placeholder='Email'
                            autoCapitalize='none'
                            autoCorrect={false}
                            keyboardType='email-address'
                            style={styles.textInput}
                            underlineColorAndroid='transparent'
                            value={this.state.email}
                            onChangeText={(text) => this.setState({ email: text })}
                            onBlur={() => this.checkInput(this.state.email, "email")} />
                        <Text style={styles.hintText}>An activation link will be sent to this email</Text>
                        <Text style={styles.errorText}>{this.state.erroremail}</Text>
                    </View>
                    <Text style={styles.termsText}>By clicking Submit,I agree that I have read and accepted the </Text>
                    <TouchableOpacity>
                        <Text style={styles.termsLink}>Terms and Conditions</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.loginBtn} onPress={() => this.checkSubmit()}>
                        <Text style={styles.loginText}>SUBMIT </Text></TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingTop: 30,
        margin: 1,

    },
    signInBar: {
        height: '5%',
        backgroundColor: '#0775a8',
        justifyContent: 'center',
    },
    signInBarText: {
        color: 'white',
        fontSize: 18,
        marginLeft: 10,
    },
    inputContainer: {
    },
    textInput: {
        width: '100%',
        height: 40,
        borderWidth: 1,
        borderRadius: 3,
        borderColor: '#cecccc'

    },
    form: {
        margin: 10,

    },
    loginBtn:
    {
        width: '100%',
        height: '8%',
        backgroundColor: '#008bd0',
        borderRadius: 3,
        alignItems: 'center',
        marginTop: '3%'
    },
    loginText: {
        color: 'white',
        margin: 10,
        fontWeight: 'bold'
    },
    termsText: {
        fontSize: 12,
    },
    termsLink: {
        color: '#0775a8',
        fontSize: 12
    },
    hintText: {
        color: '#909090',
        fontSize: 12
    },
    errorText: {
        color: 'red'
    }
});
export default RegisterForm