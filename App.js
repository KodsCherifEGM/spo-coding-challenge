import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import RegisterForm from './src/RegisterFrom';


//this is the first component that is called
//we find the RegisterForm component also
export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <RegisterForm/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
